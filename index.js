const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
  
// Keep a global reference of the window object.
var win;

function createWindow () {
    const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize;

    // Create the browser window.
    win = new BrowserWindow({
        show: false,
        alwaysOnTop: true,
        frame: false,
        width: 400, 
        height: 250,
        minWidth: 200,
        minHeight: 200,
        x: width-400-50,
        y: height-250-50
    })

    // and load the index.html of the app.
    win.loadFile('index.html')

    // Open the DevTools.
    // win.webContents.openDevTools()

    win.once('ready-to-show', () => {
        win.show()
    })

    // Emitted when the window is closed.
    win.on('closed', () => {
        win = null
    })
}

/**
 * After initialization
 */
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // Completely quit the app if on MacOS
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // Re-create a window in the app when the dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})